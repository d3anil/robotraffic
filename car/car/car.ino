#include "signals.h"
#include "states.h"
#include "L298N.h"

#include "NewPing.h"

// Line sensors //
#define p_LSensor_LEFT  7
#define p_LSensor_RIGHT 6

#define p_LSensor_LL 9
#define p_LSensor_RR 8

// Ultra-sonic sensor //
#define p_USSensor_T 12
#define p_USSensor_R 11

// LDR sensor //
#define p_LDRSensor A0

// Motor driver (L298N) //
#define p_MotorDriver_IN1_1 31
#define p_MotorDriver_IN1_2 33
#define p_MotorDriver_IN2_1 37
#define p_MotorDriver_IN2_2 35
#define p_MotorDriver_EN1   2
#define p_MotorDriver_EN2   3

// IrDA //
#define IrDA Serial1
#define p_IrDA_Error 13

// koef //
#define k_delay 0
#define m_speed 110 //110
#define m_turn_speed 110 //100

L298N driver;

NewPing sonar(p_USSensor_T, p_USSensor_R, 10);

byte dir_state = STATE_DIR_FORWARD;
byte c_dir_state = STATE_DIR_FORWARD; //current

byte speed_state = STATE_SPEED_MOVE;
byte c_speed_state = STATE_SPEED_MOVE; // current

byte SIGNAL = SIG_GREEN;

bool RUN = false;

unsigned long long c_time = 0;
unsigned long long n_time = 0;

//===== tasks =====//
void tsk_01_IrDA();
void tsk_02_sensors();
void tsk_03_direction();
void tsk_04_speed();
bool tsk_05_ultrasonic();
void tsk_06_ldr();
//===== ----- =====//

void setup() {
  driver.attach(p_MotorDriver_IN1_1,
                p_MotorDriver_IN1_2,
                p_MotorDriver_IN2_1,
                p_MotorDriver_IN2_2,
                p_MotorDriver_EN1,
                p_MotorDriver_EN2);

  pinMode(p_LSensor_LEFT, INPUT);
  pinMode(p_LSensor_RIGHT, INPUT);
  pinMode(p_IrDA_Error, OUTPUT);

  Serial.begin(115200);
  IrDA.begin(115200);

  Serial.println("START");
}

void loop() {
  tsk_06_ldr();
  if (tsk_05_ultrasonic()) {
    tsk_01_IrDA();
    tsk_02_sensors();
    tsk_03_direction();
    tsk_04_speed();
  } else {
    driver.setSpeed(0,0);
  }
}

void tsk_01_IrDA() {
  boolean left  = digitalRead(p_LSensor_LL);
  boolean right = digitalRead(p_LSensor_RR);
  if (!(left && !right)) return;

  if (!Serial1.available()) {
    if ((millis() - n_time) > 110)
      driver.setSpeed(m_speed, m_speed);
    return;
  }

  n_time = millis();
  int d =  Serial1.read();
  Serial.print(d);
  Serial.print(" ");
  SIGNAL = d;
  Serial.println(SIGNAL);
  digitalWrite(p_IrDA_Error,0);

  if ((d == SIG_RED) || (d == SIG_RED_YELLOW) || (d == SIG_YELLOW)) {
    driver.setSpeed(0,0);
    speed_state = STATE_SPEED_STOP;
    return;
  }

  if ((d == SIG_GREEN) || (d == SIG_GREEN_BLINK)) {
    driver.setSpeed(m_speed, m_speed);
    speed_state = STATE_SPEED_MOVE;
    return;
  }

  digitalWrite(p_IrDA_Error,1);
}

void tsk_02_sensors() {
  boolean left = digitalRead(p_LSensor_LEFT);
  boolean right = digitalRead(p_LSensor_RIGHT);

  if (left == right)
      dir_state = STATE_DIR_FORWARD;
  else if (left)
      dir_state = STATE_DIR_LEFT;
  else
      dir_state = STATE_DIR_RIGHT;
}

void tsk_03_direction() {
  if (speed_state == STATE_SPEED_STOP) return;

  c_dir_state = dir_state;
  if (c_dir_state == STATE_DIR_FORWARD) {
    driver.moveForward();
    speed_state = STATE_SPEED_MOVE;
  }
  if (c_dir_state == STATE_DIR_LEFT) {
    driver.moveLeft();
    speed_state = STATE_SPEED_TURN;
  }
  if (c_dir_state == STATE_DIR_RIGHT) {
    driver.moveRight();
    speed_state = STATE_SPEED_TURN;
  }
}

void tsk_04_speed() {
  c_speed_state = speed_state;
  if (c_speed_state == STATE_SPEED_STOP) driver.setSpeed(0,0);
  if (c_speed_state == STATE_SPEED_MOVE) driver.setSpeed(m_speed, m_speed);
  if (c_speed_state == STATE_SPEED_TURN) driver.setSpeed(m_turn_speed, m_turn_speed);
}

bool tsk_05_ultrasonic() {
  int s = sonar.ping_cm();
  if (s > 0)
  if (s < 10)
    return false;
  else
    return true;
}

void tsk_06_ldr() {
  int l = analogRead(A0);
  if (l>700) {
      RUN = 0;
      delay(1000);
    }
  while (!RUN) {
    driver.setSpeed(0,0);
    int l = analogRead(A0);
    if (l>700) {
      RUN = 1;
      delay(1000);
    }
  }
}

