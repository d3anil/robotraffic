#define SIG_RED         0x00
#define SIG_RED_YELLOW  0x01
#define SIG_GREEN       0x02
#define SIG_GREEN_BLINK 0x03
#define SIG_YELLOW      0x04
#define SIG_CROSSWALK   0x05
#define SIG_STOP        0x06
