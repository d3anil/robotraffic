class L298N {
  private:
  // 1 - левый, 2 - правый //
  uint8_t p_IN1_1;
  uint8_t p_IN1_2;
  uint8_t p_IN2_1;
  uint8_t p_IN2_2;
  uint8_t p_EN1;
  uint8_t p_EN2;

  public:
  // запоминание пинов, настройка пинов, установка 0 //
  void attach(uint8_t IN1_1,
              uint8_t IN1_2,
              uint8_t IN2_1,
              uint8_t IN2_2,
              uint8_t EN1  ,
              uint8_t EN2  ) {

    p_IN1_1 = IN1_1;
    p_IN1_2 = IN1_2;
    p_IN2_1 = IN2_1;
    p_IN2_2 = IN2_2;
    p_EN1   = EN1;
    p_EN2   = EN2;

    pinMode(p_IN1_1, OUTPUT);
    pinMode(p_IN1_2, OUTPUT);
    pinMode(p_IN2_1, OUTPUT);
    pinMode(p_IN2_2, OUTPUT);
    pinMode(p_EN1,   OUTPUT);
    pinMode(p_EN2,   OUTPUT);

    digitalWrite(p_IN1_1, 0);
    digitalWrite(p_IN1_2, 0);
    digitalWrite(p_IN2_1, 0);
    digitalWrite(p_IN2_2, 0);
    digitalWrite(p_EN1,   0);
    digitalWrite(p_EN2,   0);
  }

  // установка скрости //
  void setSpeed(uint8_t left, uint8_t right) {
    analogWrite(p_EN1, left);
    analogWrite(p_EN2, right);
  }

  // остановка //
  void stop() {
    digitalWrite(p_IN1_1, 0);
    digitalWrite(p_IN1_2, 0);
    digitalWrite(p_IN2_1, 0);
    digitalWrite(p_IN2_2, 0);
  }

  // движение прямо //
  void moveForward() {
    digitalWrite(p_IN1_1, 1);
    digitalWrite(p_IN1_2, 0);
    digitalWrite(p_IN2_1, 1);
    digitalWrite(p_IN2_2, 0);
  }

  // движение назад //
  void moveBackward() {
    digitalWrite(p_IN1_1, 0);
    digitalWrite(p_IN1_2, 1);
    digitalWrite(p_IN2_1, 0);
    digitalWrite(p_IN2_2, 1);
  }

  // поворот налево с блокировкой колеса //
  void moveLeft() {
    digitalWrite(p_IN1_1, 0);
    digitalWrite(p_IN1_2, 0);
    digitalWrite(p_IN2_1, 1);
    digitalWrite(p_IN2_2, 0);
  }

  // поворот направо с блокировкой колеса //
  void moveRight() {
    digitalWrite(p_IN1_1, 1);
    digitalWrite(p_IN1_2, 0);
    digitalWrite(p_IN2_1, 0);
    digitalWrite(p_IN2_2, 0);
  }

  // разворот наместе против часовой//
  void moveSpotLeft() {
    digitalWrite(p_IN1_1, 0);
    digitalWrite(p_IN1_2, 1);
    digitalWrite(p_IN2_1, 1);
    digitalWrite(p_IN2_2, 0);
  }

  // разворот наместе по часовой//
  void moveSpotRight() {
    digitalWrite(p_IN1_1, 1);
    digitalWrite(p_IN1_2, 0);
    digitalWrite(p_IN2_1, 0);
    digitalWrite(p_IN2_2, 1);
  }
};

